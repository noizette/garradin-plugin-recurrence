<?php

namespace Paheko\Plugin\Recurrence;

use Paheko\DB;
use Paheko\Utils;
use Paheko\UserException;

class Recurrence
{

	public function __construct()
	{
	}

    public function get($id)
    {
		$db = DB::getInstance();

		$r = $db->first('SELECT *   
			FROM plugin_recurrence WHERE id = ? LIMIT 1;', (int)$id);

		if(!$r)
		{	
			throw new UserException("Pas d'écriture retournée avec cet id.");
		}

		return $r;
    }

    public function add($val)
    {
		$db = DB::getInstance();

        if ($db->test('plugin_recurrence', 'transac = ?', (int) $val))
        {
            throw new UserException('L\'écriture '.(int) $val.' fait déjà partie des favoris');
        }

		$db->insert('plugin_recurrence', ['transac' => $val]);

		return $db->lastInsertRowId();	
    }

    public function edit($id, $val)
    {
		$db = DB::getInstance();

		return $db->update('plugin_recurrence', ['transac' => $val], $db->where('id', (int)$id));
    }

    public function test($val)
    {
		$db = DB::getInstance();

		return $db->test('plugin_recurrence', 'transac = ?', (int) $val);
    }

    public function delete($id)
    {
		$db = DB::getInstance();
        
        $db->delete('plugin_recurrence', 'transac = ?', (int)$id);
        
        return true;
    }

    public function listAll()
    {
		$r = (array)DB::getInstance()->getAssoc('SELECT id, transac FROM plugin_recurrence');

		return $r;
    }

    public function listAllIds()
    {
        return (array)DB::getInstance()->get('SELECT id FROM plugin_recurrence');
    }

}