<?php

namespace Paheko;

use Paheko\Plugin\Recurrence\Recurrence;
use Paheko\Accounting\Transactions;

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_ADMIN);

$rec = new Recurrence;
$selected = f('selected');

if (!$selected || !is_array($selected)) {
	throw new UserException('Aucune écriture n\'a été sélectionnée.');
}

$csrf_key = 'recurrence_action';

// Delete transactions
$form->runIf('add', function () use ($selected, $rec) {
	foreach ($selected as $id) {
		$transaction = Transactions::get((int) $id);
		
		if (!$transaction) {
			throw new UserException('Cette écriture n\'existe pas');
		}
		
		$rec->add((int) $id);
	}
}, $csrf_key, PLUGIN_ADMIN_URL.'edit.php?ok');
	
$from = f('from');
$url_from = WWW_URL . substr($from, 1);
$tpl->assign(compact('csrf_key', 	'url_from'));

$tpl->display(PLUGIN_ROOT . '/templates/action.tpl');