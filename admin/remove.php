<?php

namespace Paheko;

use Paheko\Plugin\Recurrence\Recurrence;
use Paheko\Accounting\Transactions;

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_ADMIN);
$rec = new Recurrence;

$csrf_key = 'recurrence_remove';

$id = (int) qg('id');
$transaction = Transactions::get($id);

// Si l'écriture a été supprimée et qu'on veut la suppr de la liste ?
if (!$transaction) {
    // Truc débile pour éviter de modifier le template à la place
    $transaction['id'] = $id;
    $transaction['label'] = '[écriture non trouvée]';
}


if ($rec->test($id))
{
    // Delete transactions
    $form->runIf('delete', function () use ($rec, $id) {
		$rec->delete($id);
    }, $csrf_key, PLUGIN_ADMIN_URL);
}
else
{
    throw new UserException('L\'écriture ne fait pas partie des favoris.');
}
	



$tpl->assign(compact('transaction', 'csrf_key'));

$tpl->display(PLUGIN_ROOT . '/templates/remove.tpl');