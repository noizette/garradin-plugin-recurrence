# Plugin Récurrence pour Paheko

Plugin pour gérer des écritures d'opérations récurrentes pour le logiciel de gestion d'association Paheko ( https://garradin.eu/ - https://fossil.kd2.org/garradin ).
Source : https://gitlab.com/ramoloss/garradin-plugin-facturation


Petit plugin qui créé une liste d'écriture favorites, que l'on peut ajouter dans l'onglet « Ajouter aux favoris », en sélectionnant une recherche comptable enregistrée dans Paheko. Il suffit de cocher les cases souhaitées dans les résultat de la recherche, et d'appuyer tout en bas sur le bouton « Ajouter aux favoris ». Bien sûr il est ensuite possible de les retirer depuis la liste principales des favoris.


## Installation:
Vous pouvez télécharger l'archive sur la page des [releases](https://gitlab.com/ramoloss/garradin-plugin-recurrence/-/releases).
Placer l'archive dans le dossier plugins de votre installation Paheko.


## Fonctionnalités :
- Créer et gérer une liste d'opération favorites
- Mets en évidence un bouton « Dupliquer » pour faciliter les écritures récurrentes


Ce petit bazar est sous licence GPL 3
