{include file="_head.tpl" title="Ajouter des favoris — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="edit"}

{form_errors}
{if isset($ok)}
    <p class="block confirm">
		Les écritures ont bien été ajoutées.
    </p>
{/if}

{if !$id}
{*Liste des recherches*}

        <p class="help">
			{link href="!acc/search.php" label="Bricoles ta recherche dans Paheko"}, et sélectionnes-la ici pour choisir les écritures à ajouter aux favoris.
        </p>
	{if !count($liste) == 0}
		<table class="list">
			<thead>
				<tr>
					<th>Search</th>
					<th>Type</th>
					<th>Statut</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$liste item="recherche"}
				<tr>
					<th><a href="{$search_url}?id={$recherche.id}">{$recherche.label}</a></th>
					<td>{if $recherche.type == Entities\Search::TYPE_JSON}Avancée{else}SQL{/if}</td>
					<td>{if !$recherche.id_membre}Publique{else}Privée{/if}</td>
					<td class="actions">
						{linkbutton href="%s?id=%d"|args:$search_url,$recherche.id shape="search" label="Sélectionner"}
					</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	{else}
	<p>Aucune recherche blabla, crées-en une dans Paheko</p>
	{/if}

{else}
{*Resultats de recherche*}
<?php var_dump($count); ?>
	{if $count}
		<form method="post" action="{plugin_url file="action.php"}">
			<p class="help">{$count} écritures trouvées pour cette recherche.</p>
			<table class="list search">
				<thead>
					<tr>
						<td class="check"><input type="checkbox" title="Tout cocher/décocher" id="f_all" /><label for="f_all">Tout cocher/décocher</label></td>
						{foreach from=$result_header key="key" item="label"}
							<td>{$label}</td>
						{/foreach}
						<td></td>
					</tr>
				</thead>
				<tbody>

				{*foreach from=$result item="row"*}
				{while $row = $result->fetchArray(\SQLITE3_ASSOC) }
					<tr>
						<td>{input type="checkbox" name="selected[]" value=$row.id source=$plugin.config}</td>
						
						{foreach from=$row key="key" item="value"}
							{if $key == 'id'}
							<td class="num">
								<a href="{$admin_url}acc/transactions/details.php?id={$value}">{$value}</a>
							</td>
							{else}
							<td>
								{if $key == 'credit' || $key == 'debit'}
									{$value|raw|money:false}
								{elseif $key == 'date'}
									{$value|date_short}
								{elseif null === $value}
									<em>(nul)</em>
								{else}
									{$value}
								{/if}
							</td>
							{/if}
						{/foreach}
						<td class="actions">
							{if $row.transaction_id}
							{linkbutton shape="search" label="Détails" href="!acc/transactions/details.php?id=%d"|args:$row.transaction_id}
							{/if}
						</td>
					</tr>
				{/while}
				</tbody>
				<tfoot>
					<tr>
						<td class="check"><input type="checkbox" value="Tout cocher/décocher" id="f_all2" /><label for="f_all2">Tout cocher/décocher</label></td>
						<td class="actions" colspan="9">
							<input type="hidden" name="source" value="search" />
							<input type="hidden" name="from" value="{$self_url}" />
							{csrf_field key="recurrence_action"}
							{button type="submit" name="add" label="Ajouter les écritures sélectionnées" shape="plus"}
						</td>
					</tr>
				</tfoot>
				
			</table>
		</form>
	{else}
		<p class="block alert">
			Aucun résultat trouvé.
		</p>
	{/if}

{/if}

{include file="_foot.tpl"}
