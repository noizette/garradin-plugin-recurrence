{include file="_head.tpl" title="Écritures favorites — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

{if isset($journal)}

<table class="list multi">
	<thead>
		<tr>
			<td class="num">N°</td>
			<td>Pièce comptable</td>
			<td>Date</td>
			<th>Libellé</th>
			<td>Comptes</td>
			<td class="money">Débit</td>
			<td class="money">Crédit</td>
			<td>Libellé ligne</td>
			<td>Réf. ligne</td>
			<td></td>
		</tr>
	</thead>
	{foreach from=$journal item="transaction"}
	<tbody>
		<tr>
			<td rowspan="{$transaction.lines|count}" class="num"><a href="{$admin_url}acc/transactions/details.php?id={$transaction.id}">#{$transaction.id}</a></td>
			<td rowspan="{$transaction.lines|count}">{$transaction.reference}</td>
			<td rowspan="{$transaction.lines|count}">{$transaction.date|date_short}</td>
			<th rowspan="{$transaction.lines|count}">{$transaction.label}</th>
        <?php $i = 0; ?>
		{foreach from=$transaction.lines item="line"}
			<td>{$line.account_code} - {$line.account_label}</td>
			<td class="money">{$line.debit|raw|money}</td>
			<td class="money">{$line.credit|raw|money}</td>
			<td>{$line.label}</td>
			<td>{$line.reference}</td>
            {if $i == 0}
			<td rowspan="{$transaction.lines|count}">{linkbutton href="!acc/transactions/new.php?copy=%d"|args:$transaction.id shape="plus" label="Dupliquer cette écriture"}
            {linkbutton href="remove.php?id=%d"|args:$transaction.id shape="delete" label="Enlever"}</td>
            <?php $i++ ?>
            {/if}
		</tr>
		<tr>
		{/foreach}
		</tr>
	</tbody>
	{/foreach}
</table>
{else}
<p>Aucune écriture en favoris pour le moment. Ajoutes-en dans l'onglet {link href="edit.php" label="« Ajouter des favoris »"} pour commencer !</p>
{/if}

{include file="_foot.tpl"}
