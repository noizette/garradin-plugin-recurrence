{include file="_head.tpl" title="Enlever des favoris l'écriture n°%d — %s"|args:$transaction.id,$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

{include file="common/delete_form.tpl"
	legend="Enlever cette écriture des favoris ?"
	warning="Êtes-vous sûr de vouloir enlever des vos favoris l'écriture n°%d « %s » ?"|args:$transaction.id,$transaction.label
	csrf_key=$csrf_key|args:$transaction.id
}

{include file="_foot.tpl"}